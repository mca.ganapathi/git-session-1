terraform {
    required_providers{

grafana = {
source = "grafana/grafana"
version = "1.14.0"

}
}
}


provider "grafana" {
 url = "http://3.108.139.187:3000/"
 auth = var.grafana_auth
}

variable "grafana_auth" {
      default = "eyJrIjoiMTlNM1RtZUxzZm1hQ2pKZkhzZ0pTcW01dTNsTFp4bW4iLCJuIjoiVGVycmFmb3JtIiwiaWQiOjF9"

}

